#!./venv/bin/python

import codecs
import os

from setuptools import setup

package_name = "importtools"
min_python_version = (3, 6)
tools = ["linker", "writer"]

base = os.path.abspath(os.path.dirname(__file__))

try:
    from pip._internal.req import parse_requirements
except ImportError:
    try:
        from pip.req import parse_requirements
    except ImportError:
        raise RuntimeError("Could not find pip requirements parser")


def read_file(path):
    """Read file and return contents"""
    try:
        with codecs.open(path, "r", encoding="utf-8") as f:
            return f.read()
    except IOError:
        return ""


def get_reqs(*targets):
    """Parse requirements.txt files and return array"""
    for target in targets:
        reqs = parse_requirements(
            os.path.join(base, "requirements.d", ("%s.txt" % target)),
            session="hack")
        yield from (
            r.requirement if hasattr(r, "requirement") else str(r.req)
            for r in reqs)


setup(
    name=package_name,
    version="0.2",

    description="The tools (linker, writer) used for the import into the histHub norm database.",
    long_description=read_file(os.path.join(base, "README.md")),

    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        ],

    install_requires=list(get_reqs("importtools")),

    namespace_packages=["importtools"],
    packages=(
        ["importtools.common"] + ["importtools.%s" % tool for tool in tools]),
    python_requires=(">=%s" % ".".join(map(str, min_python_version))),
    scripts=[("bin/%s" % script) for script in (
        "linker",
        "writer"
        )])
