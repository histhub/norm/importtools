import logging

from copy import deepcopy
from collections import (Counter, namedtuple, defaultdict)
from eavlib.core import EntitySpan

from importtools.common.bridge_tool import BridgeTool
from importtools.common.linking_map import LinkingMap
from importtools.linker.reference_manager import ReferenceManager
from importtools.common.util import timed

LOGGER = logging.getLogger(__name__)


# Helper function

def add_attribute(eav_model, entity_span, attr_name):
    """Add an attribute (EntitySpan.Anode) to the base entity of the entity
    span. Given an entity span like for example:

        Appellation { [lang text] labels {[lang text]} }

    calling this function will return a new entity span like:

        Appellation { [id lang text] labels {[lang text]} }

    leaving the original untouched.
    """
    new_span = deepcopy(entity_span)
    mod_attr = eav_model.entity(new_span.namid.id).attribute(attr_name)
    new_span.enode.amap[attr_name] = EntitySpan.Anode(mod_attr)
    return new_span


class LinkError(Exception):
    pass


class HistHubLinker(BridgeTool):
    """The linker will search instances of the given entity types in the
    staging area and using different methods try to find entities in the main
    histhub repository corresponding to the incoming staging ones.

    The found concordances are written into a linking map table-like structure
    like the one bellow:

       staging    entity    obj.    linking     main      histHub
       eav id     name      type    state       eav id    id
       -------    ------    ----    --------    ------    ----------
       100015     Person    ref     linked      800345    12045963
       100027     Person    info    unlinked
       100043     Person    ref     intra-link  100027
       100077     Place     ref     linked      801074    13006214
       100091     Place     info    linked      802033    13005478
       ...

    where "linked" means that an object in the histHub area is found to be
    either the counterpart of an information object in the staging area or the
    target of a reference. "unlinked" means that no histHub counter part where
    found, and thus the target and histhub id columns are empty. In the case of
    staging reference objects, the "inta-link" state mean that they are
    pointing to objects defined in the submitted data; the target EAV id in
    this case is a staging area id, meaning that the writer will give assign
    this reference the same histhub id as the staging area pointed object.

    There is no "error" state; objects having some problems are reported in the
    logs and their linking map entry is meaningless.

    The linker tries to find as many errors as possible. It proceeds logging
    found errors as further as possible until an error is found that prevents
    any sensible continuation. In case of errors an exception is thrown,
    otherwise a linking map object is returned.

    """
    FingerPrintData = namedtuple('FPD', ('entity_span', 'allow_duplicates'))
    SIMILARITY_FINGERPRINTS_MAP = {
        # allow_duplicates=False  means that duplicated objects submitted to
        # the staging area (not already known in histHub) are considered the
        # same and reported as intra-links; later the writer will create on-
        # ly one object representing all of them.  This is the usually desi-
        # red behavior,  except for names, since in the case two persons are
        # named "John Doe" we want to have these names as dfferent objects.

        'Appellation': FingerPrintData(
            allow_duplicates=False,
            entity_span='''
                Appellation {
                    [text lang gender]
                }'''),

        'DataSource': FingerPrintData(
            allow_duplicates=False,
            entity_span='''
                DataSource {
                    [caption]
                    institution {[name]}
                    revision {[name]}
                    reference {
                        [article biblio excerpt lang
                         line page topoform url volume]
                        date {
                            [calendar day mode month size type unit year]
                            notes {[lang text]}
                        }
                    }
                }'''),

        'Institution': FingerPrintData(
            allow_duplicates=False,
            entity_span='''
                Institution {
                    [name]
                }'''),

        'License': FingerPrintData(
            allow_duplicates=False,
            entity_span='''
                License {
                    [name version]
                }'''),

        'PersonName': FingerPrintData(
            allow_duplicates=True,
            entity_span='''
                PersonName {
                    []
                    forenames {
                        [order]
                        forename {
                            [text lang gender]
                        }
                    }
                    surnames {
                        [order]
                        namelink {
                            [text lang gender]
                        }
                        surname {
                            [text lang gender]
                        }
                    }
                }'''),

        'PlaceName': FingerPrintData(
            allow_duplicates=True,
            entity_span='''
                PlaceName {
                    [text lang gender]
                    notes {[lang text]}
                    reference {[]}
                }'''),

        'FamilyName': FingerPrintData(
            allow_duplicates=True,
            entity_span='''
                FamilyName {
                    names {
                        [order]
                        namelink {[text lang gender]}
                        surname {[text lang gender]}
                        notes {[lang text]}
                    }
                    notes {[lang text]}
                }'''),

        'OrganizationName': FingerPrintData(
            allow_duplicates=True,
            entity_span='''
                OrganizationName {
                    [text lang gender]
                    notes {[lang text]}
                }'''),

        'Geometry': FingerPrintData(
            allow_duplicates=False,
            entity_span='''
                Geometry {
                    [coordinates srid]
                    notes {[lang text]}
                }'''),

        'FreeTextLocation': FingerPrintData(
            allow_duplicates=False,
            entity_span='''
                FreeTextLocation {
                    definitions {
                        [text lang]
                        notes {[lang text]}
                    }
                    notes {[lang text]}
                }'''),

        'TopographicReference': FingerPrintData(
            allow_duplicates=False,
            entity_span='''
                TopographicReference {
                    [biblio excerpt lang topoform]
                    date {
                        [calendar day mode month size type unit year]
                        notes {[lang text]}
                    }
                }'''),

        'URLReference': FingerPrintData(
            allow_duplicates=False,
            entity_span='''
                URLReference {
                    [lang url]
                    date {
                        [calendar day mode month size type unit year]
                        notes {[lang text]}
                    }
                }'''),

        'VolumeReference': FingerPrintData(
            allow_duplicates=False,
            entity_span='''
                VolumeReference {
                    [article lang line page volume]
                    date {
                        [calendar day mode month size type unit year]
                        notes {[lang text]}
                    }
                }'''),

        'ExternalReference': FingerPrintData(
            allow_duplicates=False,
            entity_span='''
                ExternalReference {
                    reference {
                        [article biblio excerpt lang line page topoform url
                         volume]
                        date {
                            [calendar day mode month size type unit year]
                            notes {[lang text]}
                        }
                    }
                }'''),
        }

    def __init__(self, histhub_graph, staging_graph):
        super().__init__(histhub_graph, staging_graph)
        self._found_errors = False
        self._main_ref_manager = ReferenceManager(histhub_graph)
        self._stag_ref_manager = ReferenceManager(staging_graph)

    def find_anchor_concordances(self):
        return self.find_concordances(self.anchor_entities)

    def find_identifiable_concordances(self):
        return self.find_concordances(tuple(filter(
            lambda entity_name: entity_name in self.anchor_entities,
            self.identifiable_entities)))

    @timed(LOGGER)
    def find_concordances(self, entity_list):
        """Main entry point of the general linker.

        Find concordances for the passed entities and reports them with a
        LinkingMap instance.

            entity_list     list of entity names
        """
        try:
            self._check_entities_to_link(entity_list)
            linking_map = self._initialize_linking_map(entity_list)
            self._build_linking_map_from_references(linking_map, entity_list)
            self._build_linking_map_from_similarity(linking_map, entity_list)

            return linking_map
        except RuntimeError as err:
            self._error(err)
            raise LinkError("There were errors during the link process.")

    def _error(self, error_msg, *args, **kwargs):
        LOGGER.error(error_msg, *args, **kwargs)
        self._found_errors = True

    def _initialize_linking_map(self, entity_list):
        """Insert a default entry for any object to be linked in the staging
        area.
        """
        entity_ids_map = {
            self._stag_data_model.entity(entity_name).id: entity_name
            for entity_name in entity_list}
        LOGGER.debug("entity_ids_map = %s", entity_ids_map)

        linking_map = LinkingMap()
        # Populate the linking map with "unlinked" records for found objects.
        # No further records are added later; only state changes are made.
        for t in self._staging_dm.read_objects_for_eids(entity_ids_map.keys()):
            linking_map.add(
                t[0], entity_ids_map[t[1]], LinkingMap.ObjType.info)

        LOGGER.debug("initialized linking map =\n%s", linking_map)
        return linking_map

    def _check_entities_to_link(self, entity_list):
        """Checks that the given entities belong to the ontology being used and
        that they are linkable entities.
        """
        # Check that entities to link are defined in the ontology or model
        unknown_entities = list(filter(
            lambda n: not self._main_data_model.has_entity(n), entity_list))
        if unknown_entities:
            raise RuntimeError(
                "The following entity names are unknown: %s" % (
                    ", ".join(unknown_entities)))

        # Catch for entities that the linker does not deal with
        non_allowed_entities = list(filter(
            lambda e: e not in self.anchor_entities, entity_list))
        if non_allowed_entities:
            raise RuntimeError(
                "The following entity names are not linked: %s" % (
                    ", ".join(non_allowed_entities)))

    def _build_linking_map_from_references(self, linking_map, entity_list):
        LOGGER.info("Linking by external ID")
        for entity_name in entity_list:
            self._resolve_references_for_class(entity_name, linking_map)

    def _build_linking_map_from_similarity(self, linking_map, entity_list):
        LOGGER.info("Linking by similarity")
        for entity_name in entity_list:
            self._find_similarities_for_class(entity_name, linking_map)

    def _resolve_references_for_class(self, entity_name, linking_map):
        LOGGER.info("Linking entity %s (external ID)", entity_name)

        # Find outgoing (obj_2_ref) and incoming (ref_2_obj) references
        obj_2_ref, ref_2_obj = self._stag_ref_manager.bidirectional_maps(
            entity_name)
        LOGGER.debug("obj_2_ref = %s", obj_2_ref)
        LOGGER.debug("ref_2_obj = %s", ref_2_obj)
        if not obj_2_ref:
            return

        reference_solver = self._main_ref_manager.reference_solver(entity_name)
        obj_type_decider = self._stag_ref_manager.object_type_decider(
            entity_name)

        # Find and mark reference objects in linking map.
        for idpair, refs_list in obj_2_ref.items():
            if obj_type_decider.object_type(
                    idpair.eav_id, refs_list) is LinkingMap.ObjType.ref:
                linking_map[idpair.eav_id].set_as_reference_object()

        # ref_2_obj scan for intra-links.
        #
        # A reference can appear in multiple reference objects, but it cannot
        # appear in more than one information objects (that is those defining
        # real data, and not merely referencing other objects).  In this case
        # we have DUPLICATED objects in the staging area. This is independent
        # of the reference being solved or not.
        #
        # Entries in the ref_2_obj map look like: Reference -> [ id_pair list ]
        #   2:Place:14055789     -> [ (None,5412) ]
        #   3:Place:ONCH/4055789 -> [ (None,7665), (None,5412), (None,3227) ]
        for ref, obj_list in ref_2_obj.items():
            if len(obj_list) > 1:
                obj_type_map = defaultdict(list)
                for idpair in obj_list:
                    obj_type_map[linking_map[idpair.eav_id].object_type] \
                        .append(idpair.eav_id)
                LOGGER.debug("{} -> {}".format(ref, obj_type_map))
                if len(obj_type_map[LinkingMap.ObjType.info]) > 1:
                    # more than one information objects share the same ref
                    #   ObjType.info -> [ eid1, eid2, ..., eidM ]
                    #   ObjType.ref  -> don't care
                    self._error(
                        "duplicated objects: %s share the same reference %s",
                        ", ".join(map(
                            str, obj_type_map[LinkingMap.ObjType.info])),
                        ref)
                elif len(obj_type_map[LinkingMap.ObjType.info]) == 1:
                    # several reference objects share a ref with one infor-
                    # mation object.  If the ref is not resolved, we are in
                    # the case of an intra-link, that is, internal referen-
                    # ces between objects being defined inside a submission
                    # that are not yet known by histHub since they are just
                    # arriving.
                    #   ObjType.info -> [ eidK ]
                    #   ObjType.ref  -> [ eid1, ..., eidM ]
                    if not reference_solver.is_solved(ref):
                        info_eav_id = obj_type_map[LinkingMap.ObjType.info][0]
                        for eid in obj_type_map[LinkingMap.ObjType.ref]:
                            linking_map[eid].set_as_intra_link(info_eav_id)
                # else:
                    # The case where all objects are reference objects is
                    # taken care below when transfering information to the
                    # linking map.
                    #   ObjType.info -> [ ]
                    #   ObjType.ref  -> [ eid1, eid2, ..., eidN ]
                    # pass
        del ref_2_obj

        # obj_2_ref traverse to transfer information to the linking map.
        #
        # Entries in the linking map get states and ids set according to re-
        # fererence resolution. Till now, the only states that were set, are
        # the intra-links.
        #
        # Entries in the obj_2_ref map look like:
        #
        #  (None,5412) ->  [ 2:Place:14055789, 3:Place:ONCH/4055789 ]
        #  (None,1304) ->  [ 3:Place:SSRQ/loc003045 ]
        #
        for idpair, ref_list in obj_2_ref.items():
            link_entry = linking_map[idpair.eav_id]

            # An object with no references is unlinked and always an INFO
            # object.
            #    ipd -> [ ]
            if len(ref_list) < 1:
                link_entry.set_as_unlinked()

            # An object with only one reference migth be an INFO or a REF
            # object.
            #    ipd -> [ ref1 ]
            elif len(ref_list) == 1:
                if reference_solver.is_solved(ref_list[0]):
                    # When solved, state is always linked, irrespective of
                    # object type.
                    resol = reference_solver.solution(ref_list[0])
                    link_entry.set_as_linked(resol.eav_id)
                    LOGGER.debug(
                        "reference resolution link: %s(%s), eav=%s, hhid=%s, "
                        "ref=%s", entity_name, idpair.eav_id, resol.eav_id,
                        resol.histhub_id, ref_list[0])
                else:
                    if link_entry.is_reference_object():
                        # In the case of a REF object that was previously mar-
                        # ked as "intra-link" leave as it is. Otherwise an
                        # error must by logged since we found a dangling refe-
                        # rence.
                        if not link_entry.is_intra_link():
                            self._error(
                                "dangling reference object: %s(%s), ref=%s",
                                entity_name, idpair.eav_id, ref_list[0])
                    else:
                        # Nothing to do in case of an INFO object, since the
                        # default entry state is "unlinked".
                        pass

            # An object with several references is always an INFO object.
            #    ipd -> [ ref1, ref2, ..., refN ]
            else:
                # len(ref_list) > 1,  obj_2_ref scan for inconsistent refs.
                #
                # The list of references that information objects contain can
                # be checked to find duplicated ones (not really an error, but
                # reported as such to get clean data), and inconsistent ones
                # that is, references of an object that point to different
                # histHub instances.
                sol_ref_map = {
                    r: reference_solver.solution(r)
                    for r in ref_list if reference_solver.is_solved(r)}

                resolved_values = set(sol_ref_map.values())
                refs_text = ', '.join(sol_ref_map.keys())
                if len(resolved_values) > 1:
                    self._error(
                        "inconsistent references %s in object %s",
                        refs_text, idpair.eav_id)
                elif len(resolved_values) == 1:
                    resol = next(iter(resolved_values))
                    link_entry.set_as_linked(resol.eav_id)
                    LOGGER.debug(
                        "reference resolution link: %s(%s), eav=%s, hhid=%s, "
                        "refs=[%s]", entity_name, idpair.eav_id,
                        resol.eav_id, resol.histhub_id, refs_text)
                else:
                    # Nothing to do in case of an INFO object, since the
                    # default entry state is "unlinked".
                    pass

                ref_counts = Counter(ref_list)
                for ref in ref_counts:
                    if ref_counts[ref] > 1:
                        self._error(
                            "duplicated references: %s appears %u times in "
                            "object %s", ref, ref_counts[ref], idpair.eav_id)

        if self._found_errors:
            tmp = "found errors when solving references for entity '{}'"
            raise RuntimeError(tmp.format(entity_name))

    def _find_similarities_for_class(self, entity_name, linking_map):
        """Link all entities of a given class by similarity of their
        fingerprints.
        """
        try:
            fpdata = self.SIMILARITY_FINGERPRINTS_MAP[entity_name]

            LOGGER.info("Linking entity %s (similarity)", entity_name)
            self._link_by_fingerprint_match(
                entity_name, linking_map, fpdata.entity_span,
                fpdata.allow_duplicates)
        except KeyError:
            return

    def _link_by_fingerprint_match(self, entity_name, linking_map, span_text,
                                   allow_duplicates=True):
        allow_duplicates = bool(allow_duplicates)
        stag_fp_span = self._stag_data_model.entity_span(span_text)
        stag_vicinity = stag_fp_span.to_vicinity()

        # Find out ids for staging area objects that are unlinked (for the
        # given entity type), and load them quitting if nothing found.
        unlinked_ids = linking_map.unlinked_ids(entity_name)
        if not unlinked_ids:
            return
        staging_map = self._staging_dm.read_vicinity(
            stag_vicinity, unlinked_ids)
        if not staging_map:
            return

        # Load all instances in the main area of the given type and index them.
        # It is assumed that all of them have a proper id.
        main_fp_span = self._main_data_model.entity_span(span_text)
        main_vicinity = main_fp_span.to_vicinity()
        histhub_map = self._histhub_dm.read_vicinity(main_vicinity)
        histhub_fp_map = {}  # { fp -> main_obj }
        for main_obj in histhub_map.values():
            fingerprint = main_fp_span.set_fingerprint(main_obj)
            histhub_fp_map[fingerprint] = main_obj
            # LOGGER.debug("fingerprint in main area {}, mid={} fp='{}'"
            #     .format(entity_name, main_obj.eav_id, fingerprint))

        # Compute fingerprints for staging area objects. If they match one of
        # the previously computed histHub object fingerprints, we have a link
        # Check also if a staging area fingerprint was also seen before for
        # the case an object is defined more than once.
        unlinked_fp_map = {}  # { fp -> stag_obj }
        for stag_id, stag_object in staging_map.items():
            stag_fingerprint = stag_fp_span.set_fingerprint(stag_object)

            # if the fingerprint has been seen before but was unlinked (new
            # object defined more than once in staging area)
            if stag_fingerprint in unlinked_fp_map:
                if allow_duplicates:
                    linking_map[stag_id].set_as_unlinked()
                    LOGGER.debug(
                        "fingerprint resolution: unlinked %s, stag=%s, "
                        "fp='%s'", entity_name, stag_id, stag_fingerprint)
                else:
                    # no duplicates allowed; newly found objects are
                    # intra-linked.
                    prev_eav_id = unlinked_fp_map[stag_fingerprint]
                    linking_map[stag_id].set_as_intra_link(prev_eav_id)
                    LOGGER.debug(
                        "fingerprint resolution: intra-link %s, stag=%s, "
                        "target_stag=%s fp='%s'", entity_name, stag_id,
                        prev_eav_id, stag_fingerprint)

            # If the fingerprint is known in histHub we have a link.
            # It doesn't matter whether it was seen before in the staging area
            # or not)
            elif stag_fingerprint in histhub_fp_map:
                histhub_obj = histhub_fp_map[stag_fingerprint]
                linking_map[stag_id].set_as_linked(histhub_obj.eav_id)
                LOGGER.debug(
                    "fingerprint resolution: linked %s, stag=%s, main=%s, "
                    "hhbid=%s, fp='%s'", entity_name, stag_id,
                    histhub_obj.eav_id, None, stag_fingerprint)

            # it is the first time we see this unknown fingerprint. Issue an
            # 'unlinked' entry and keep memory of it for the case of repeated
            # objects sharing its fate.
            else:
                linking_map[stag_id].set_as_unlinked()
                unlinked_fp_map[stag_fingerprint] = stag_id
                LOGGER.debug(
                    "fingerprint resolution: unlinked %s, stag=%s, fp='%s'",
                    entity_name, stag_id, stag_fingerprint)


def _dump_reference_maps(entity_name, obj_2_ref, ref_2_obj):
    template = "REFERENCE {} {:<20} {:<30} -> [ {} ]"
    for stag_id, refs in obj_2_ref.items():
        LOGGER.debug(template.format(
            'ob2rf', entity_name, stag_id, '  '.join(refs)))
    for ref, ids in ref_2_obj.items():
        LOGGER.debug(template.format(
            'rf2ob', entity_name, ref, ', '.join(map(str, ids))))
