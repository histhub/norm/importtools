from collections import (defaultdict, namedtuple)
from psycopg2.extras import NamedTupleCursor

from importtools.linker.histhub_reference import HhbRef

IdPair = namedtuple('IdPair', ('histhub_id', 'eav_id'))


class ReferenceManager:
    pname_query = """
        -- {INAME_CHAR}:Institution:{{prov_name}}
        SELECT DISTINCT
            CAST(hhbid.value AS integer) AS "histhub_id"
           ,ent.id AS "eav_id"
           ,'{INAME_CHAR}:Institution:'||pname.value AS "reference"
        FROM {SCHEMA_NAME}.object ent
        LEFT JOIN {SCHEMA_NAME}.object_attribute hhbid
        ON ent.id = hhbid.object_id
        AND hhbid.attribute_id = (
            SELECT a.id
            FROM {SCHEMA_NAME}.model_entity e
            JOIN {SCHEMA_NAME}.model_attribute a ON e.id = a.entity_id
            WHERE e.name = 'Identifiable'
              AND a.name = 'hhb_id'
              AND e.model_id = {MODEL_ID}
        )
        JOIN {SCHEMA_NAME}.object_attribute pname
        ON ent.id = pname.object_id
        AND pname.attribute_id = (
            SELECT a.id
            FROM {SCHEMA_NAME}.model_entity e
            JOIN {SCHEMA_NAME}.model_attribute a ON e.id = a.entity_id
            WHERE e.name = 'Institution'
              AND a.name = 'name'
              AND e.model_id = {MODEL_ID}
        )
        JOIN {SCHEMA_NAME}.model_entity me ON ent.entity_id = me.id
        WHERE me.name IN %s;
    """

    hhbid_query = """
        -- {HHBID_CHAR}:{{entity_name}}:{{histhub_id}}
        SELECT DISTINCT
            CAST(hhbid.value AS integer) AS "histhub_id"
           ,ent.id AS "eav_id"
           ,'{HHBID_CHAR}:'||me.name||':'||hhbid.value AS "reference"
        FROM {SCHEMA_NAME}.object ent
        LEFT JOIN {SCHEMA_NAME}.object_attribute hhbid
        ON ent.id = hhbid.object_id
        AND hhbid.attribute_id = (
            SELECT a.id
            FROM {SCHEMA_NAME}.model_entity e
            JOIN {SCHEMA_NAME}.model_attribute a ON e.id = a.entity_id
            WHERE e.name = 'Identifiable'
              AND a.name = 'hhb_id'
              AND e.model_id = {MODEL_ID}
        )
        JOIN {SCHEMA_NAME}.model_entity me ON ent.entity_id = me.id
        WHERE me.name IN %s;
    """

    extid_query = """
        -- {EXTID_CHAR}:{{entity_name}}:{{prov_name}}/{{external_id}}
        SELECT DISTINCT
            CAST(hhbid.value AS integer) AS "histhub_id"
           ,ent.id AS "eav_id"
           ,'{EXTID_CHAR}:'||me.name
           ||':'||pname.value
           ||'/'||atteid.value AS "reference"
        FROM {SCHEMA_NAME}.object ent
        LEFT JOIN {SCHEMA_NAME}.object_attribute hhbid
        ON ent.id = hhbid.object_id
        AND hhbid.attribute_id = (
            SELECT a.id
            FROM {SCHEMA_NAME}.model_entity e
            JOIN {SCHEMA_NAME}.model_attribute a ON e.id = a.entity_id
            WHERE e.name = 'Identifiable'
              AND a.name = 'hhb_id'
              AND e.model_id = {MODEL_ID}
        )
        JOIN {SCHEMA_NAME}.object_relation rext
        ON ent.id = rext.object_id AND rext.relation_id = (
            SELECT r.id
            FROM {SCHEMA_NAME}.model_entity e
            JOIN {SCHEMA_NAME}.model_relation r ON e.id = r.source_id
            WHERE e.name = 'HistHubEntity'
              AND r.name = 'external_ids'
              AND e.model_id = {MODEL_ID}
        )
        JOIN {SCHEMA_NAME}.object_attribute atteid
        ON rext.target_id = atteid.object_id AND atteid.attribute_id = (
            SELECT a.id
            FROM {SCHEMA_NAME}.model_entity e
            JOIN {SCHEMA_NAME}.model_attribute a ON e.id = a.entity_id
            WHERE e.name = 'ExternalIdentifier'
              AND a.name = 'external_id'
              AND e.model_id = {MODEL_ID}
        )
        JOIN {SCHEMA_NAME}.object_relation inst
        ON rext.target_id = inst.object_id AND inst.relation_id = (
            SELECT r.id
            FROM {SCHEMA_NAME}.model_entity e
            JOIN {SCHEMA_NAME}.model_relation r ON e.id = r.source_id
            WHERE e.name = 'ExternalIdentifier'
              AND r.name = 'institution'
              AND e.model_id = {MODEL_ID}
        )
        JOIN {SCHEMA_NAME}.object_attribute pname
        ON inst.target_id = pname.object_id AND pname.attribute_id = (
            SELECT a.id
            FROM {SCHEMA_NAME}.model_entity e
            JOIN {SCHEMA_NAME}.model_attribute a ON e.id = a.entity_id
            WHERE e.name = 'Institution'
              AND a.name = 'name'
              AND e.model_id = {MODEL_ID}
        )
        JOIN {SCHEMA_NAME}.model_entity me ON ent.entity_id = me.id
        WHERE me.name IN %s;
    """

    degree_query = """
        -- Get the number of attributes and relations of objects
        WITH
            queried_entities(eid) AS (
                SELECT e.id
                FROM {SCHEMA_NAME}.model_entity e
                WHERE e.model_id = {MODEL_ID}
                  AND e.name IN %s
            ),
            number_of_attributes(eav_id, num) AS (
                SELECT o.id, count(a.*)
                FROM {SCHEMA_NAME}.object o
                JOIN queried_entities e ON o.entity_id = e.eid LEFT
                JOIN {SCHEMA_NAME}.object_attribute a ON o.id = a.object_id
                GROUP BY o.id
            ),
            number_of_relations(eav_id, num) AS (
                SELECT o.id, count(r.*)
                FROM {SCHEMA_NAME}.object o
                JOIN queried_entities e ON o.entity_id = e.eid LEFT
                JOIN {SCHEMA_NAME}.object_relation r ON o.id = r.object_id
                GROUP BY o.id
            )
        SELECT
            na.eav_id AS "eav_id"
           ,na.num AS "num_atts"
           ,nr.num AS "num_rels"
        FROM number_of_attributes na
        JOIN number_of_relations nr ON na.eav_id = nr.eav_id;
    """

    def __init__(self, graph):
        self.conn = graph.connection

        subs = {
            'INAME_CHAR': HhbRef.Type.INAME.value,
            'HHBID_CHAR': HhbRef.Type.HHBID.value,
            'EXTID_CHAR': HhbRef.Type.EXTID.value,
            'SCHEMA_NAME': self.conn.schema,
            'MODEL_ID': graph.model_id,
            }
        self.query_for = {
            HhbRef.Type.INAME: self.pname_query.format(**subs),
            HhbRef.Type.HHBID: self.hhbid_query.format(**subs),
            HhbRef.Type.EXTID: self.extid_query.format(**subs),
            'outgoing_degree': self.degree_query.format(**subs),
            }

    @staticmethod
    def _load_direct_refs(connection, query, entity_names, result_map):
        with connection.cursor(cursor_factory=NamedTupleCursor) as cursor:
            cursor.execute(query, (entity_names,))
            connection.rollback()
            for t in cursor.fetchall():
                if t.reference:
                    result_map[IdPair(t.histhub_id, t.eav_id)].append(
                        t.reference)

    @staticmethod
    def _load_reverse_refs(connection, query, entity_names, result_map):
        with connection.cursor(cursor_factory=NamedTupleCursor) as cursor:
            cursor.execute(query, (entity_names,))
            connection.rollback()
            for t in cursor.fetchall():
                if t.reference:
                    result_map[t.reference].append(IdPair(
                        t.histhub_id, t.eav_id))

    @staticmethod
    def _entity_names_list(entity_spec):
        """Check if entity_spec is a string or a list of strings. No check
        is made whether the strings are actual entity names. A tuple is
        always returned.
        """
        if isinstance(entity_spec, str):
            return (entity_spec,)
        elif isinstance(entity_spec, (list, tuple)):
            if all(isinstance(e, str) for e in entity_spec):
                return entity_spec
        raise RuntimeError("expecting an entity names or a list of names")

    @staticmethod
    def _ref_types_list(ref_type_spec):
        if ref_type_spec is None:
            return list(HhbRef.Type.__members__.values())
        elif isinstance(ref_type_spec, HhbRef.Type):
            return [ref_type_spec]
        elif isinstance(ref_type_spec, (list, tuple)):
            if all(isinstance(t, HhbRef.Type) for t in ref_type_spec):
                return set(ref_type_spec)
        raise RuntimeError(
            "expecting None, a HhbRef.Type instance or a list of them")

    def _populate_map(self, map_2_fill, load_func, entity_spec, ref_type_spec):
        """Populates a dictionary with the references information delivered
        back by the given load function for the specified entities and
        reference types.
        """
        entity_names = ReferenceManager._entity_names_list(entity_spec)
        ref_types = ReferenceManager._ref_types_list(ref_type_spec)

        # INAME references only have sense for the Institution entity
        if HhbRef.Type.INAME in ref_types and 'Institution' in entity_names:
            load_func(self.conn, self.query_for[HhbRef.Type.INAME],
                      ('Institution',), map_2_fill)
        if HhbRef.Type.HHBID in ref_types:
            load_func(self.conn, self.query_for[HhbRef.Type.HHBID],
                      entity_names, map_2_fill)
        if HhbRef.Type.EXTID in ref_types:
            load_func(self.conn, self.query_for[HhbRef.Type.EXTID],
                      entity_names, map_2_fill)
        return map_2_fill

    def direct_map(self, entity_spec, ref_type_spec=None):
        """Loads a map like: (hhb_id, eav_id) -> [ references list ]
        for the specified entities and reference types.
        """
        return self._populate_map(defaultdict(list),
                                  ReferenceManager._load_direct_refs,
                                  entity_spec,
                                  ref_type_spec)

    def reverse_map(self, entity_spec, ref_type_spec=None):
        """Loads a map like: reference -> [ (hhb_id, eav_id) list ]
        for the specified entities and reference types.
        """
        return self._populate_map(defaultdict(list),
                                  ReferenceManager._load_reverse_refs,
                                  entity_spec,
                                  ref_type_spec)

    def bidirectional_maps(self, entity_spec, ref_type_spec=None):
        """Returns both maps: (direct, reverse)."""
        return (
            self.direct_map(entity_spec, ref_type_spec),
            self.reverse_map(entity_spec, ref_type_spec))

    def outgoing_degree_map(self, entity_spec):
        """Returns a map like:
           eav_id -> (num_atts, num_rels)

        It expresses the number of outgoing edges objects have, broken down in
        relations and attributes.
        """
        entity_names = ReferenceManager._entity_names_list(entity_spec)

        with self.conn.cursor(cursor_factory=NamedTupleCursor) as cursor:
            cursor.execute(self.query_for['outgoing_degree'], (entity_names,))
            self.conn.rollback()

            return {t.eav_id: (t.num_atts, t.num_rels) for t in cursor}

    def reference_solver(self, entity_spec, ref_type_spec=None):
        return ReferenceSolver(self.reverse_map(entity_spec, ref_type_spec))

    def object_type_decider(self, entity_spec, ref_type_spec=None):
        return StagObjTypeDecider(self.outgoing_degree_map(entity_spec))


class ReferenceSolver:
    """Translates references into (hhb_id, eav_id) pairs."""

    def __init__(self, reverse_map):
        """The reverse_map is like:
            { reference -> [ (hhb_id, eav_id) list ] }
        and is accepted if it has no ambiguity.
        """
        wrong = list(filter(lambda _: len(reverse_map[_]) != 1, reverse_map))
        if wrong:
            raise RuntimeError("wrong references: {}".format('\n'.join(
                "{} -> {}".format(r, [i.eav_id for i in reverse_map[r]])
                for r in wrong)))
        self.ref_map = {ref: reverse_map[ref][0] for ref in reverse_map}

    def is_solved(self, reference):
        return reference in self.ref_map

    def solution(self, reference):
        return self.ref_map[reference]


class StagObjTypeDecider:
    """Tells if an object in the staging area is a reference to some histHub
    entity or an object carrying information to be loaded.
    """

    def __init__(self, outgoing_degree_map):
        """A map showing the number of outgoing edges for an object, that is
        number of attributes and relations:
           eav_id -> (num_atts, num_rels)
        """
        self.degree_map = outgoing_degree_map

    def object_type(self, eav_id, refs_list):
        """The object type is decided by the number of references an object
        has and the number of outgoing attributes and relations.
        """
        return HhbRef.object_type(
            refs_list,
            self.degree_map[eav_id][0],
            self.degree_map[eav_id][1])
