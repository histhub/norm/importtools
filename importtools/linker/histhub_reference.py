from enum import Enum
from importtools.common.linking_map import LinkingMap


class HhbRef:
    """histHub references are strings that codify the reference type, the entity
    name of the referenced object, and some reference type dependant
    information.

    For example:

        1:Institution:{prov_name}
        2:{entity_name}:{histhub_id}
        3:{entity_name}:{prov_name}/{external_id}

    The HhbRef class offers some method to synthesize such references or
    recover some of their components.
    """
    SEP = ':'

    class Type(Enum):
        INAME = 'I'
        HHBID = 'H'
        EXTID = 'X'

    @staticmethod
    def from_provider_name(prov_name):
        return "{}{}Institution{}{}".format(
            HhbRef.Type.INAME.value,
            HhbRef.SEP,
            HhbRef.SEP,
            prov_name)

    @staticmethod
    def from_histhub_id(entity_name, histhub_id):
        return "{}{}{}{}{}".format(
            HhbRef.Type.HHBID.value,
            HhbRef.SEP,
            entity_name,
            HhbRef.SEP,
            histhub_id)

    @staticmethod
    def from_external_id(entity_name, prov_name, external_id):
        return "{}{}{}{}{}/{}".format(
            HhbRef.Type.EXTID.value,
            HhbRef.SEP,
            entity_name,
            HhbRef.SEP,
            prov_name,
            external_id)

    @classmethod
    def type(cls, ref_string):
        return cls.Type(ref_string.split(cls.SEP)[0])

    @classmethod
    def entity(cls, ref_string):
        return ref_string.split(cls.SEP)[1]

    @classmethod
    def identifier(cls, ref_string):
        return ref_string.split(cls.SEP)[2]

    @classmethod
    def object_type(cls, refs_list, num_atts, num_rels):
        """A staging area object is a reference when it contains only and
        exclusively the information needed to express the reference. If
        anything addtionally is present, it is considered an information
        object.
        """
        if len(refs_list) == 1:
            if cls.type(refs_list[0]) in (cls.Type.INAME, cls.Type.HHBID):
                if num_atts == 1 and num_rels == 0:
                    return LinkingMap.ObjType.ref
            elif cls.type(refs_list[0]) == cls.Type.EXTID:
                if num_atts == 0 and num_rels == 1:
                    return LinkingMap.ObjType.ref

        return LinkingMap.ObjType.info
