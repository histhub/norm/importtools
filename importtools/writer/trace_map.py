from enum import Enum
from typing import (Iterator, List)

from eavlib.core.eav_bag import EavBag


class TraceMap:
    """A map of EAV objects keyed by their anchor traces, that can be used to
    track whether other objects match them or are different to those already
    in the map.
    """
    State = Enum('State', ('unmatched', 'matched', 'inserted'))

    class Entry:
        """Ties together an EAV object and its matching status."""
        __slots__ = ('obj', 'state')

        def __init__(self, obj, state=None):
            self.obj = obj
            self.state = TraceMap.State.unmatched if state is None else state

        def __repr__(self):
            return "{},{}".format(self.state, self.obj)

    def __init__(self, list_of_bags: List[EavBag]):
        """EAV bags in the list need their anchor traces been set."""
        self._map = {obj.anchor_trace: self.Entry(obj)
                     for obj in list_of_bags}

    def knows(self, trace: str) -> bool:
        """True if the given trace is contained in this map."""
        return trace in self._map

    def eav_bag(self, trace: str) -> EavBag:
        """The EAV bag associated to this trace."""
        return self._map[trace].obj

    def set_as_matched(self, trace) -> None:
        """Sets the object associated to the given trace as matched."""
        if self.knows(trace):
            self._map[trace].state = self.State.matched

    def set_as_inserted(self, obj: EavBag) -> None:
        """Associates the given trace to the given object in inserted state."""
        self._map[obj.anchor_trace] = self.Entry(obj, self.State.inserted)

    def unmatched_objects(self) -> Iterator[EavBag]:
        """Return the EAV object for those traces in unmatched state."""
        return map(lambda a: a.obj, filter(
            lambda e: e.state == self.State.unmatched,
            self._map.values()))
