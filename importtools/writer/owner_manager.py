from typing import Dict
from copy import deepcopy
from itertools import chain
from collections import namedtuple

from eavlib.core import (ChangeBatch, EavModel)
from eavlib.core.eav_bag import EavBag


class OwnerManager:
    """Instance implementing the histHub owner annotation policy.

    OwnerManager instances keep track of provider information as an object in
    the main repository is created or being merged with information coming in
    another object read from the staging area.

    Two maps {object_id -> set of owner ids} are kept, one with the state on
    creation time and a second one keeping changes done subsequently. Initia-
    lly the managed owner (id) is removed from all owners set, so that at the
    end, an empty owner set signals that a node has to be deleted (it didn't
    come in the staging area node). For unchanged nodes or those newly added,
    the owner set is updated. Later the "save_owners" method comparing the 2
    maps can flush the needed changes in a ChangeBatch instance in order to
    be persisted.

    Each map value also keeps track whether a node belong to a class the inhe-
    rits from "Owned".
    """

    OWNED_TRAIT = 'Owned'
    OWNER_RELATION = 'owners'

    Entry = namedtuple('OwnerManagerEntry', ('owners', 'is_owned'))

    def __init__(self, owners_bag: EavBag, managed_owner_id: int,
                 ontology_model: EavModel) -> None:
        """The owners_bag is a _ bag containing a web of related sub-objects
        with owners information, like for example:

            MainObject(600){
                owners = [
                    Institution(101){name=I1},
                    Institution(102){name=I2}
                ]
                relation1 = [
                    OwnedObject(601) {
                        owners = [ Institution(102){name=I2} ]
                    }
                ]
                relation2 = [
                    NonOwnedObject(602) { }
                ]
            }
        """
        self.root_entity_id = owners_bag.eav_id
        self.managed_owner_id = managed_owner_id
        self.ontology_model = ontology_model
        self.owners_rel_id = ontology_model \
            .entity(self.OWNED_TRAIT) \
            .relation(self.OWNER_RELATION).id

        self.prior_owner_map = self._build_onwers_map(owners_bag)

        self.after_owner_map = deepcopy(self.prior_owner_map)
        # Remove the managed owner id from all entries in the after map. Nodes
        # that get a provider assignement will be saved when the "save_owners"
        # method is invoked.
        for entry in self.after_owner_map.values():
            if self.managed_owner_id in entry.owners:
                entry.owners.remove(self.managed_owner_id)

    def _build_onwers_map(self, owners_bag: EavBag) -> Dict[
            int, "OwnerManager.Entry"]:
        """Turn an EAV bag with provider information into a map where keys are
        node ids and values sets of owner ids for that nodes and an annotation
        whether to object is owned or not.

        Given the input EAV bag:

            MainObject(600){
                owners = [
                    Institution(101){name=I1},
                    Institution(102){name=I2}
                ]
                relation1 = [
                    OwnedObject(601) {
                        owners = [ Institution(102){name=I2} ]
                    }
                ]
                relation2 = [
                    NonOwnedObject(602) { }
                ]
            }

        produces:

            600 -> { 101, 102 }, is_owned=True
            601 -> { 102 },      is_owned=True
            602 -> { },          is_owned=False

        Sets in the returned map are never None, although they might be empty.

        Recursion is not needed since provider information is always at levels
        0 and 1 and the passed bag has only depth 1. Extension of the provider
        annotation to deeper levels wouldn't require a change here though.
        """
        def _recursive_build_owners_map(eav_bag, result_map):
            if self._is_owned(eav_bag.entity_name):
                owners = eav_bag.get_related(self.OWNER_RELATION)
                if owners:
                    result_map[eav_bag.eav_id] = OwnerManager.Entry(
                        set(map(lambda p: p.eav_id, owners)), True)
                else:
                    result_map[eav_bag.eav_id] = OwnerManager.Entry(
                        set(), True)
            else:
                result_map[eav_bag.eav_id] = OwnerManager.Entry(set(), False)

            for relation_name in eav_bag.relations:
                if relation_name != self.OWNER_RELATION:
                    for sub_object in eav_bag.get_related(relation_name):
                        _recursive_build_owners_map(sub_object, result_map)

        owners_map = {}
        _recursive_build_owners_map(owners_bag, owners_map)
        return owners_map

    def add_owner_to_all_nodes(self) -> None:
        """Add the given provider to all provider sets in this manager."""
        for node_id in self.after_owner_map.keys():
            self.after_owner_map[node_id].owners.add(self.managed_owner_id)

    def add_owner(self, node_id: int) -> None:
        """Add the managed owner to the given node ID."""
        self.after_owner_map[node_id].owners.add(self.managed_owner_id)

    def set_owner(self, new_id: int, entity_name: str) -> None:
        """Add a new entry in the after map for the given id for an object
        whose class is the given entity name. This is intended for objects
        that are newly created.
        """
        self.after_owner_map[new_id] = OwnerManager.Entry(
            set((self.managed_owner_id,)),
            self._is_owned(entity_name))

    def has_no_owner(self, node_id: str) -> bool:
        """Whether a node is left without owners in the after map."""
        return not bool(self.after_owner_map[node_id].owners)

    def set_owners_to_root(self) -> None:
        """Sets the owners for the root node being managed by this owner
        manager, by collection all owners in all nodes.
        """
        self.after_owner_map[self.root_entity_id] = OwnerManager.Entry(
            set(chain.from_iterable(map(
                lambda v: v.owners,
                self.after_owner_map.values()))),
            self.after_owner_map[self.root_entity_id].is_owned)

    def save_owners(self, change_batch: ChangeBatch) -> None:
        """Records instructions to add or remove providers in the passed chan-
        ge batch for EAV nodes found in the after owner map.
        """
        self.set_owners_to_root()
        for node_id, after_entry in self.after_owner_map.items():
            if after_entry.is_owned:
                if node_id not in self.prior_owner_map:
                    # A new node was added
                    for owner_id in after_entry.owners:
                        change_batch.write_relation(
                            node_id, self.owners_rel_id, owner_id)
                else:
                    prior_set = self.prior_owner_map[node_id].owners
                    for owner_id in prior_set.difference(after_entry.owners):
                        change_batch.delete_relation_instance(
                            node_id, self.owners_rel_id, owner_id)
                    for owner_id in after_entry.owners.difference(prior_set):
                        change_batch.write_relation(
                            node_id, self.owners_rel_id, owner_id)

    def _is_owned(self, entity_name: str) -> bool:
        """Whether an entity is "Owned" or not."""
        return self.ontology_model.inherits_from(entity_name, self.OWNED_TRAIT)
