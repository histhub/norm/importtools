# Writer metadata

__description__ = (
    "The merger merges information from a staging eav schema into the "
    "central histHub repository.")

__author__ = "SSRQ-SDS-FDS Law Sources Foundation of the Swiss Lawyers Society"
__copyright__ = ("Copyright 2019-2021 %s" % __author__)
__license__ = "GPLv3"
__maintainer__ = __author__
# Versions as per PEP 440 (https://www.python.org/dev/peps/pep-0440/)
__version_info__ = (0, 2)
__version__ = "0.2"
