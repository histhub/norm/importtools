import enum
import itertools
import logging

from typing import TYPE_CHECKING
from functools import lru_cache

from eavlib.core import EavBag
from eavlib.core import EntitySpan
from eavlib.core import ChangeBatch
from eavlib.core import ModelVicinity
from eavlib.core import DataManager
from normlib import HistHubID

from importtools.common.anchor_trace import (anchor_trace, translate_ids)
from importtools.common.bridge_tool import BridgeTool
from importtools.common.linking_map import LinkingMap
from importtools.common.util import timed
from importtools.writer.owner_manager import OwnerManager
from importtools.writer.trace_map import TraceMap

if TYPE_CHECKING:
    from typing import (Iterator, Optional)

LOGGER = logging.getLogger(__name__)


class LinkingMapError(Exception):
    pass


class HistHubWriter(BridgeTool):
    HOOKS = enum.Enum("WriterHook", (
        # hook - arguments
        "ENTITY_INSERTION",     # object_id, entity_name, histhub_id
        "ENTITY_NO_CHANGE",     # object_id, entity_name, histhub_id
        "ENTITY_UPDATE",        # object_id, entity_name, histhub_id
        "SUBOBJECT_INSERTION",  # object_id, entity_name, histhub_id
        "SUBOBJECT_NO_CHANGE",  # object_id, entity_name, histhub_id
        "SUBOBJECT_DELETION",   # object_id, entity_name, histhub_id
        ))

    def __init__(self, norm_db, staging_graph, linking_map,
                 provider_name=None):
        super().__init__(norm_db.graph, staging_graph)

        self.hhb_id_manager = norm_db.id_manager()

        self.owners_rel_id = self._main_data_model.entity(
            self.OWNED_TRAIT).relation(self.OWNED_TRAIT_REL).id
        self.histhub_id_aid = self._main_data_model.entity(
            self.HHBID_TRAIT).attribute(self.HHBID_TRAIT_ATTR).id

        self.owned_entities = {e.name for e in self._main_data_model.entity(
            self.OWNED_TRAIT).all_children}

        self._linking_map = linking_map
        self._entities_to_merge = linking_map.all_entity_names()
        self._provider_id = self._lookup_provider(provider_name)
        self._hooks = {hook: [] for hook in self.HOOKS}

    def register_hook(self, hook, callback):
        if not callable(callback):
            raise ValueError("callback is not callable")

        self._hooks[self.HOOKS(hook)].append(callback)

    def _call_hooks(self, hook, *args, **kwargs):
        for cb in self._hooks.get(self.HOOKS(hook), ()):
            cb(*args, **kwargs)

    @timed(LOGGER)  # noqa: C901
    def run(self):
        """Run the writer process.

        1. Every (linkable) entity that the linker wasn't able to link is
           considered new and an empty object shell is created (with a new
           histHub ID, if applicable) for it.

        2. For every Identifiable linked entity the histHub ID is copied from
           the histHub main database to the staging area.
           If the histHub ID is not present in the staging area, the writer's
           safety checks will trigger and abort due to the different
           attributes.

        3. All intra_link entries in the linking map are rewritten to the
           object ID in the main database that they reference.  In addition
           their state is changed to linked so that they will go through the
           merging process and subtle differences in the objects that were
           undetected by the linker will get noticed.

        4. For each type of linkable entity (=anchor entity), do the
           merging/writing process, distiguishing between the histHub primary
           entities, that are merged with ownership information, and the others
           (without ownership).

           For each entity:
           a. First, new (=unlinked) objects are copied "as they are" from the
              staging area.
           b. Then, the linked objects are merged.
        """

        try:
            self._touched_objects = {}

            # Step 1
            self._create_new_objects()

            # Step 2
            self._copy_hhids_to_staging_area()

            # Step 3
            self._translate_intra_links()

            # Step 4
            entities_to_merge = self.anchor_entities & \
                self._linking_map.all_entity_names()
            for entity_name in entities_to_merge:
                LOGGER.info("Processing entity %s", entity_name)

                provider_id = self._provider_id \
                    if self._is_owned(entity_name) else None

                # NOTE: unlinked linking map entries need to be processed
                # before the linked ones for intra-links tobe processed
                # correctly.
                i = 0
                for entry in self._linking_map.entries_for(
                        state='unlinked', otype='info', ename=entity_name):
                    try:
                        self._write_entity(entry, provider_id)
                    except Exception:
                        LOGGER.error("Writing entity %r failed.", entry)
                        raise
                    else:
                        i += 1
                        if i % 1000 == 0:
                            LOGGER.info(
                                "  %u %s entities written", i, entity_name)
                LOGGER.info("  %u %s entities written", i, entity_name)

                i = 0
                for entry in self._linking_map.entries_for(
                        state='linked', otype='info', ename=entity_name):
                    try:
                        self._merge_entity(entry, provider_id)
                    except Exception:
                        LOGGER.error("Merging entity %r failed.", entry)
                        raise
                    else:
                        i += 1
                        if i % 1000 == 0:
                            LOGGER.info(
                                "  %u %s entities merged", i, entity_name)
                LOGGER.info("  %u %s entities merged", i, entity_name)

            empty_objects = {k for k, v in self._touched_objects.items()
                             if v == "created"}
            if empty_objects:
                LOGGER.error(
                    "%u empty objects have been created but seemingly never "
                    "used: main=%r", len(empty_objects), empty_objects)
        except Exception:
            self._histhub_dm.rollback()
            raise
        else:
            self._histhub_dm.commit()

    def _next_eav_id(self) -> int:
        """Retrieve the next object ID from the DB sequence."""
        return next(self._histhub_dm._id_generator)

    def _next_eav_ids(self, count: int) -> "Iterator[int]":
        """Retrieve multiple object IDs from the DB sequence."""
        return self._histhub_dm._id_generator.take(count)

    def _next_histhub_id(self) -> HistHubID:
        """Retrieve the next histHub id from the corresponding sequence."""
        return self.hhb_id_manager.request_id()

    def _insert_histhub_id(self, stag_hhb_id):
        """Logic for dealing with histHub id insertions.

        Returns a valid histhub id or throws an exception if the passed one
        is unusable.
        """
        if stag_hhb_id is None:
            return self._next_histhub_id()
        else:
            # TODO:
            # A check is needed here to determine if the given id is usable.
            # In the negative case, an alternative id is to be returned or an
            # error raised (TBD); currently allowing everything.
            return HistHubID(stag_hhb_id)

    def _merge_histhub_ids(self, stag_hhb_id: "Optional[int]",
                           main_hhb_id: int) -> int:
        """Logic for dealing with histHub ID updates.

        :param stag_hhb_id: the current histHub ID in the staging database.
        :type stag_hhb_id: int
        :param main_hhb_id: the current histHub ID in the main database.
        :type main_hhb_id: int
        :returns: int -- a valid ID to be used
        :raises ValueError: when the `main_hhb_id` is None
        :raises RuntimeError: when the `stag_hhb_id` and `main_hhb_id` differ.
        """
        main_hhb_id = int(main_hhb_id)
        if stag_hhb_id is not None:
            stag_hhb_id = int(stag_hhb_id)

        if main_hhb_id is None:
            raise ValueError("main_hhb_id must not be None")
        if stag_hhb_id is not None and stag_hhb_id != main_hhb_id:
            raise RuntimeError("Trying to overwrite hhb_id %u with %u" % (
                main_hhb_id, stag_hhb_id))

        return main_hhb_id

    def _is_owned(self, entity_name):
        """True if the given entity inherits from Owned."""
        # return self._main_data_model.inherits_from(
        #     entity_name,       # child
        #     self.OWNED_TRAIT)  # parent
        return entity_name in self.owned_entities

    def _is_identifiable(self, eav_bag):
        """True if the provided bag belongs to a class having histhub ids."""
        # XXX[DC]: Lookup in identifiable_entities?
        return self._main_data_model.inherits_from(
            eav_bag.entity_name,  # child
            self.HHBID_TRAIT)     # parent

    def _instance_histhub_id(self, eav_bag: EavBag) -> "Optional[int]":
        """Returns the histHub ID of an EAV instance if it is Identifiable,
        otherwise None.
        """
        if self._is_identifiable(eav_bag):
            return int(eav_bag.get_attribute(self.HHBID_TRAIT_ATTR))
        else:
            return None

    def merge_span(self, entity_name):
        """Computes the merging entity span for an anchor entity, e.g.:

            merge_span("Appellation")
              │
              └───> Appellation {
                        [gender is_standard lang text]
                        external_ids {
                            [external_id]
                            institution {[]}
                        }
                        references {[]}
                        notes {[]}
                        standard_form {[]}
                    }
        """
        def _subspan(entity_name, model):
            entity = model.entity(entity_name)
            rels = []
            for r in entity.all_relations:
                if r.name == self.OWNED_TRAIT_REL:
                    continue
                if r.target_entity.name in self.anchor_entities:
                    sub_span = '{[]}'
                else:
                    sub_span = _subspan(r.target_entity.name, model)
                rels.append("%s %s" % (r.name, sub_span))

            attr_text = " ".join(sorted(a.name for a in entity.all_attributes))
            rels_text = " ".join(sorted(rels))
            return "{[%s] %s }" % (attr_text, rels_text)

        return "%s %s" % (entity_name, _subspan(
            entity_name, self._main_data_model))

    def owner_span(self, ename):
        """Computes the span used to load provider information of an entity,
        e.g.:

            owner_span("Place", eav_model)
              │
              └───> Place { owners {[]}
                       used_names { owners {[]} }
                       used_types { owners {[]} }
                       used_locations { owners {[]} }
                       relations { owners {[]} }
                    }
        """
        subspan = '%s {[]}' % self.OWNED_TRAIT_REL
        model = self._main_data_model
        owned_obj = model.entity(self.OWNED_TRAIT)
        entity = model.entity(ename)
        rels_text = []
        for rel in entity.all_relations:
            if rel.name == self.OWNED_TRAIT_REL:
                continue
            if owned_obj in rel.target_entity.all_parents:
                rels_text.append(f"{rel.name} {{ [] {subspan} }}")
        return "{} {{ [] {} {} }}".format(ename, subspan, ' '.join(rels_text))

    @lru_cache()
    def main_entity_span(self, entity_name: str) -> EntitySpan:
        """Returns an entity span to be used in the histhub repository."""
        return self._main_data_model.entity_span(self.merge_span(entity_name))

    @lru_cache()
    def main_entity_vicinity(self, entity_name: str) -> ModelVicinity:
        """Returns an entity vicinity to be used in the histhub repository."""
        return self.main_entity_span(entity_name).to_vicinity()

    @lru_cache()
    def main_ownership_vicinity(self, entity_name: str) -> ModelVicinity:
        """Returns a vicinity to retrieve ownership information for the given
        entity (always from the histHub repository).
        """
        return self._main_data_model.entity_span(
            self.owner_span(entity_name)).to_vicinity()

    @lru_cache()
    def stag_entity_vicinity(self, entity_name: str) -> ModelVicinity:
        """Returns an entity vicinity to be used in the staging repository."""
        return self._stag_data_model.entity_span(
            self.merge_span(entity_name)).to_vicinity()

    def _lookup_provider(self, provider_name):
        """Given a provider name, looks up its histHub ID, returning it or
        raising an exception if the provider is not known.  If the provider
        name is None, checks whether entities with provider annotation are to
        be merged, raising an exception in this case.
        """
        if provider_name:
            provider_span_text = "Institution {[name]}"
            provider_vicinity = self._main_data_model.entity_span(
                provider_span_text).to_vicinity()
            provider_names_map = {
                # The name attribute should be unique for providers.
                p.get_attribute('name'): i
                for i, p in self._histhub_dm.read_vicinity(
                    provider_vicinity, subclasses=True).items()
                }
            try:
                provider_id = provider_names_map[provider_name]
            except KeyError:
                message = f"Unknown provider name '{provider_name}'"
                LOGGER.error(message)
                raise RuntimeError(message)

        else:
            # Return None if the provider name is empty, except for the
            # cases with entities that are merged with provider
            # annotatation where an exception is raised.
            needing_provider = self._entities_to_merge & self.owned_entities
            if needing_provider:
                template = "a provider name is needed in order to merge: {}"
                message = template.format(', '.join(needing_provider))
                LOGGER.error(message)
                raise RuntimeError(message)
            else:
                provider_id = None
        LOGGER.debug("Given provider '%s' mapped to main eav id=%s",
                     provider_name, provider_id)
        return provider_id

    def _create_new_objects(self):
        """Entities that the linker was not able to link are considered new and
        empty objects for them are created now.
        """
        entries_it = self._linking_map.entries_for(
            state="unlinked", otype="info")

        def _batch():
            # batch size = 1000
            return tuple(itertools.islice(entries_it, 1000))

        batch = _batch()
        if not batch:
            return

        LOGGER.info("Creating new objects for unlinked info objects")

        while batch:
            create_batch = ChangeBatch()

            log_msg = "Creating new objects for:"
            try:
                emap = {e.stag_id: e for e in batch}
                new_objects = self._staging_dm.read_objects_for_oids(emap)
                id_it = self._next_eav_ids(len(new_objects))

                for (object_id, entity_id) in new_objects:
                    entry = emap[object_id]

                    try:
                        entry.eav_id = next(id_it)
                    except StopIteration:
                        # NOTE: This should never happen!"
                        # If this code ever executes, fix the id_it.
                        LOGGER.debug(
                            "%s._create_new_objects: exhausted histHub ID "
                            "cache." % (__name__))
                        entry.eav_id = self._next_eav_id()

                    create_batch.write_object(entry.eav_id, entity_id)
                    self._touched_objects[entry.eav_id] = "created"

                    log_msg += "\n%s(%s) for staging id %s" % (
                        self._stag_data_model.entity(entity_id).name,
                        entry.eav_id, entry.stag_id)
            finally:
                if locals().get("emap", None):
                    del emap
                if locals().get("new_objects", None):
                    del new_objects

                LOGGER.debug(log_msg)

            self._histhub_dm.execute_batch(create_batch)
            del create_batch

            batch = _batch()

    def _copy_hhids_to_staging_area(self):
        """Copy the histhub ids to the staging area.

        Entities that were linked might not have the histHub ID attribute
        in the staging area (for example, ThesaurusConcept instances that
        are linked exclusively by TM IDs). These IDs are essential for
        comparing objects or subobjects.
        """
        hhb_id_aid_stag = self._stag_data_model.entity(
            self.HHBID_TRAIT).attribute(self.HHBID_TRAIT_ATTR).id
        hhb_id_aid_main = self._main_data_model.entity(
            self.HHBID_TRAIT).attribute(self.HHBID_TRAIT_ATTR).id

        linked_entries = tuple(self._linking_map.entries_for(
            state="linked", otype="info",
            ename=list(self.identifiable_entities)))
        if not linked_entries:
            return

        LOGGER.info("Copying histHub IDs to staging area")

        stag_ids, main_ids = zip(*(
            (e.stag_id, e.eav_id) for e in linked_entries))

        main_values = {
            oid: aval
            for oid, aid, aval in self._histhub_dm.read_attributes_for(
                main_ids, (hhb_id_aid_main,))
            }
        stag_values = {
            oid: aval
            for oid, aid, aval in self._staging_dm.read_attributes_for(
                stag_ids, (hhb_id_aid_stag,))
            }

        stag_change_batch = ChangeBatch()

        for stag_id, main_id in zip(stag_ids, main_ids):
            stag_val, main_val = (stag_values.get(stag_id, None),
                                  main_values.get(main_id, None))

            new_stag_val = self._merge_histhub_ids(stag_val, main_val)
            if new_stag_val != stag_val:
                stag_change_batch.write_attribute(
                    stag_id, hhb_id_aid_stag, str(new_stag_val))
                stag_values[stag_id] = new_stag_val

        for entry in self._linking_map.entries_for(
                state="intra_link", otype="info",
                ename=list(self.identifiable_entities)):
            try:
                # NOTE: Since this is an intra_link, the eav_id is a staging ID
                stag_change_batch.write_attribute(
                    entry.stag_id, hhb_id_aid_stag, stag_values[entry.eav_id])
            except KeyError:
                # looks like a duplicate in the data…
                # These cases have to be handled when merging the objects since
                # the histHub IDs are not known, yet.
                pass

        self._staging_dm.execute_batch(stag_change_batch)

    def _translate_intra_links(self):
        """Intra links are references to objects only known in the staging area.
        Now that these referenced unknown objects got their brand-new histHub
        IDs, the intra links have to be updated into proper histHub IDs.
        """
        LOGGER.info('Translating intra-link references')
        for current_entry in self._linking_map.entries_for(state='intra_link'):
            pointed_entry = self._linking_map[current_entry.eav_id]
            current_entry.eav_id = pointed_entry.eav_id
            current_entry.state = LinkingMap.State.linked

    def _write_entity(self, linking_entry, provider_id):
        try:
            stag_bag = self.load_staging_object(linking_entry)
            translate_ids(stag_bag, self.anchor_entities, self._linking_map)
        except Exception:
            raise RuntimeError(
                "staging entity not found for linking entry: '%r'",
                linking_entry)

        if (stag_bag.eav_id in self._touched_objects  # eav_id is main ID
                and self._touched_objects[stag_bag.eav_id] != "created"):
            raise RuntimeError(
                "Tried to write entity (main=%d) multiple times." % (
                    stag_bag.eav_id))

        change_batch = ChangeBatch()
        main_obj_id = self.insert_histhub_object(change_batch, stag_bag)
        self._histhub_dm.execute_batch(change_batch)
        self._touched_objects[main_obj_id] = "written"

        if self._is_owned(stag_bag.entity_name):
            if not provider_id:
                raise ValueError(
                    "Cannot write staging object %d without provider_id" % (
                        stag_bag.eav_id))
            provider_batch = ChangeBatch()
            prov_bag = self.load_histhub_ownership(linking_entry)
            owner_mgr = OwnerManager(
                prov_bag, provider_id, self._histhub_dm.eav_model())
            owner_mgr.add_owner_to_all_nodes()
            owner_mgr.save_owners(provider_batch)
            self._histhub_dm.execute_batch(provider_batch)

        histhub_id = self._instance_histhub_id(stag_bag)
        self._log_merge_action(linking_entry,
                               'new insertion',
                               provider_id,
                               histhub_id)
        self._call_hooks(self.HOOKS.ENTITY_INSERTION,
                         object_id=stag_bag.eav_id,
                         entity_name=linking_entry.entity_name,
                         histhub_id=self._instance_histhub_id(stag_bag))

    def _merge_entity(self, linking_entry: LinkingMap.Entry, provider_id: int):
        try:
            # Load the entity to merge as it is in the main area
            # and as it comes in the staging area.
            main_bag = self.load_histhub_object(linking_entry)
            stag_bag = self.load_staging_object(linking_entry)

            if main_bag.entity_name != stag_bag.entity_name:
                raise LinkingMapError("Cannot link %s with %s" % (
                    stag_bag.entity_name, main_bag.entity_name))

            is_owned = self._is_owned(main_bag.entity_name)

            if is_owned and not provider_id:
                raise ValueError(
                    "Cannot merge owned entity without provider_id")

            # Patch up histHub IDs in the staging object in case they were
            # intra_links (AAAGH!), in which case the "original" object has
            # been written already and thus has received a histHub ID, but not
            # the duplicates of it, because the histHub ID was not know at
            # step 2 (copy histHub IDs to staging area).
            # FIXME: This section of the code is a hack and based on the
            # assumption that Identifiable entities are always anchor entities.
            histhub_id = self._instance_histhub_id(main_bag)
            if histhub_id:
                stag_hhb_id = stag_bag.get_attribute(self.HHBID_TRAIT_ATTR)
                stag_hhb_id_should = self._merge_histhub_ids(
                    stag_hhb_id, histhub_id)
                if stag_hhb_id != stag_hhb_id_should:
                    stag_bag.set_attribute(
                        self.HHBID_TRAIT_ATTR, str(stag_hhb_id_should))

                    cb = ChangeBatch()
                    cb.write_attribute(
                        stag_bag.eav_id,
                        self._stag_data_model.entity(
                            self.HHBID_TRAIT).attribute(
                                self.HHBID_TRAIT_ATTR).id,
                        str(stag_hhb_id_should))
                    self._staging_dm.execute_batch(cb)
                    del cb
                del stag_hhb_id, stag_hhb_id_should
        except Exception as e:
            raise RuntimeError("merging %s: %r", linking_entry, e)

        # XXX: anchors is empty because we’re only interested in equality of
        # information, correct?
        stag_trace = anchor_trace(stag_bag, set())
        main_trace = anchor_trace(main_bag, set())

        are_same_entities = stag_trace == main_trace

        if (main_bag.eav_id in self._touched_objects
                and self._touched_objects[main_bag.eav_id] != "created"):
            if are_same_entities:
                LOGGER.error(
                    "Tried to merge multiple %s objects with a single object "
                    "(main=%d). Ignoring because they are the same.",
                    main_bag.entity_name, main_bag.eav_id)
            else:
                raise RuntimeError(
                    "Tried to merge multiple %s objects with a single object "
                    "(main=%d). Erroneous case tried to merge %r with %r" % (
                        main_bag.entity_name, main_bag.eav_id,
                        stag_trace, main_trace))

        if are_same_entities:
            self._log_merge_action(linking_entry,
                                   'same entities, nothing done',
                                   provider_id,
                                   histhub_id)
            self._call_hooks(self.HOOKS.ENTITY_NO_CHANGE,
                             object_id=main_bag.eav_id,
                             entity_name=linking_entry.entity_name,
                             histhub_id=histhub_id)
            return

        translate_ids(stag_bag, self.anchor_entities, self._linking_map)
        change_batch = ChangeBatch()

        self._touched_objects[main_bag.eav_id] = "merged"

        if not is_owned:
            # Merging WITHOUT owner information

            self.delete_histhub_object(change_batch, main_bag)
            # NOTE: Reinsertion will reuse the main node ID because
            #       translate_ids() has replaced stag_bag.eav_id with the main
            #       node ID already and insert_histhub_object uses it.
            self.insert_histhub_object(change_batch, stag_bag)

            self._log_merge_action(linking_entry,
                                   'entity rewritten (deleted & inserted)',
                                   provider_id,
                                   histhub_id)
            self._call_hooks(self.HOOKS.ENTITY_UPDATE,
                             object_id=main_bag.eav_id,
                             entity_name=linking_entry.entity_name,
                             histhub_id=histhub_id)
        else:
            # Merging WITH owner information
            entity_updated = False

            # Load provider information from the main area
            prov_bag = self.load_histhub_ownership(linking_entry)

            owner_mgr = OwnerManager(
                prov_bag, provider_id, self._histhub_dm.eav_model())

            # Merge attributes. There is no ownership for attributes,
            # so they get overwritten by the latest writer.
            # XXX[DC]: Is this case allowed? When objects are owned they
            # should not change attributes, correct?
            for attribute in self._main_data_model.entity(
                    linking_entry.entity_name).all_attributes:
                stag_val = stag_bag.get_attribute(attribute.name)
                main_val = main_bag.get_attribute(attribute.name)
                if attribute.name == self.HHBID_TRAIT_ATTR:
                    stag_val = self._merge_histhub_ids(stag_val, main_val)
                if stag_val is not None:
                    change_batch.write_attribute(
                        main_bag.eav_id,
                        attribute.id,
                        stag_val)
                elif main_val is not None:
                    change_batch.delete_attribute(
                        main_bag.eav_id,
                        attribute.id)
                owner_mgr.add_owner(main_bag.eav_id)

            # For all kinds of relations this instance could have.
            for relation in self._main_data_model.entity(
                    linking_entry.entity_name).all_relations:

                if relation.name == self.OWNED_TRAIT_REL:
                    # Owners are taken care differently.
                    continue

                owned_target = self._is_owned(relation.target_entity.name)
                trace_map = TraceMap(main_bag.get_related(relation.name))

                # For all sub-objects related to the staging instance by a
                # particular relation, find out whether they match a sub-object
                # of the main instance, or otherwise are new information, to be
                # inserted.

                for stag_obj in stag_bag.get_related(relation.name):
                    stag_trace = stag_obj.anchor_trace
                    if trace_map.knows(stag_trace):
                        # No new information, just update the owner.
                        main_eav_id = trace_map.eav_bag(stag_trace).eav_id
                        trace_map.set_as_matched(stag_trace)
                        if owned_target:
                            owner_mgr.add_owner(main_eav_id)
                        self._log_merge_action(
                            linking_entry,
                            "same sub-object (rel=%s id=%u), nothing done" % (
                                relation.name, main_eav_id),
                            provider_id, histhub_id)
                        self._call_hooks(self.HOOKS.SUBOBJECT_NO_CHANGE,
                                         object_id=main_eav_id,
                                         entity_name=linking_entry.entity_name,
                                         histhub_id=histhub_id)
                    else:
                        # New information:
                        # - insert the node,
                        # - link it to its parent,
                        # - update the owner.
                        new_object_id = self.insert_histhub_object(
                            change_batch, stag_obj)
                        change_batch.write_relation(
                            linking_entry.eav_id, relation.id, new_object_id)
                        stag_obj.eav_id = new_object_id
                        trace_map.set_as_inserted(stag_obj)
                        if owned_target:
                            owner_mgr.set_owner(new_object_id,
                                                stag_obj.entity_name)
                        entity_updated = True
                        self._log_merge_action(
                            linking_entry,
                            "new sub-object (rel=%s id=%u)" % (
                                relation.name, new_object_id),
                            provider_id, histhub_id)
                        self._call_hooks(self.HOOKS.SUBOBJECT_INSERTION,
                                         object_id=new_object_id,
                                         entity_name=linking_entry.entity_name,
                                         histhub_id=histhub_id)

                # For all sub-objects related to the main instance by a
                # particular relation, find out whether they have to be deleted
                # because no match was found for them and they don't belong to
                # other provider.

                for main_subobj in trace_map.unmatched_objects():
                    delete_obj = owner_mgr.has_no_owner(main_subobj.eav_id) \
                        if owned_target else True
                    if delete_obj:
                        self.delete_histhub_object(change_batch, main_subobj)
                        change_batch.delete_relation_instance(
                            linking_entry.eav_id,
                            relation.id,
                            main_subobj.eav_id)
                        self._log_merge_action(
                            linking_entry,
                            "deleted sub-object (rel=%s id=%u)" % (
                                relation.name, main_subobj.eav_id),
                            provider_id, histhub_id)
                        entity_updated = True
                        self._call_hooks(self.HOOKS.SUBOBJECT_DELETION,
                                         object_id=main_subobj.eav_id,
                                         entity_name=linking_entry.entity_name,
                                         histhub_id=histhub_id)

            owner_mgr.save_owners(change_batch)

            if entity_updated:
                self._call_hooks(self.HOOKS.ENTITY_UPDATE,
                                 object_id=main_bag.eav_id,
                                 entity_name=linking_entry.entity_name,
                                 histhub_id=histhub_id)

        self._histhub_dm.execute_batch(change_batch)

    def load_histhub_object(self, entry: LinkingMap.Entry) -> EavBag:
        """Loads an object from the histHub repository, or raises an exception
        if not found. The object is specified by its EAV ID and its class name
        as given in a linking map entry (histHub pointer).
        """
        return self._load_vicinity(self._histhub_dm,
                                   entry.eav_id,
                                   self.main_entity_vicinity(
                                       entry.entity_name))

    def load_staging_object(self, entry: LinkingMap.Entry) -> EavBag:
        """Loads an object from the staging repository, or raises an exception
        if not found. The object is specified by its EAV ID and its class name
        as given in a linking map entry (staging pointer).
        """
        return self._load_vicinity(self._staging_dm,
                                   entry.stag_id,
                                   self.stag_entity_vicinity(
                                       entry.entity_name))

    def load_histhub_ownership(self, entry: LinkingMap.Entry) -> EavBag:
        """Loads the part of an object containing ownership information from
        the histHub repository, or raises an exception if not found.
        The object is specified by its EAV ID and its class name.
        """
        return self._load_vicinity(self._histhub_dm,
                                   entry.eav_id,
                                   self.main_ownership_vicinity(
                                       entry.entity_name))

    def _load_vicinity(self,
                       data_manager: DataManager,
                       eav_id: int,
                       vicinity: ModelVicinity) -> EavBag:
        """Loads an object, returning an EavBag or raising an exception if not
        found.
        """
        result = data_manager.read_vicinity(vicinity, (eav_id,))
        if result is None or eav_id not in result:
            err_msg = f"{data_manager.name} entity not found eav_id={eav_id}"
            LOGGER.error(err_msg)
            raise RuntimeError(err_msg)
        return result[eav_id]

    def insert_histhub_object(self,
                              change_batch: ChangeBatch,
                              eav_bag: EavBag) -> int:
        """Writes into the histHub main area all the information in an EAV bag.

        The root object (for a bag whose entity is an anchor class) is never
        written, just its attributes and its related subobjects. Any anchor
        entity found in the recursion is left untouched, since they are boun-
        dary markes of writing. Writing of anchor objects is the responsibi-
        lity of the _create_new_objects method before any merging happens.

        Returns the EAV ID of the written object.
        """
        def __recursive_data_insertion(bag, enode, level=0):
            if bag.entity_name in self.anchor_entities:
                # In case of anchor node, the staging node ID has already
                # being translated to the corresponding main node ID.
                main_eav_id = bag.eav_id
                if level > 0:
                    # Nothing to do with anchor entities downstream the root
                    return main_eav_id
            else:
                # In case of internal node, create a new database eav_id,
                # and create the node.
                main_eav_id = self._next_eav_id()
                change_batch.write_object(
                    main_eav_id,
                    self._main_data_model.entity(bag.entity_name).id)

            # Write the attributes (for all nodes that are not anchor objects
            # or for the root anchor object (never for leaves).
            # No check is required since it has been done already above.
            for anode in enode.amap.values():
                attr_value = bag.get_attribute(anode.namid.name)
                if anode.namid.name == self.HHBID_TRAIT_ATTR:
                    attr_value = self._insert_histhub_id(attr_value)
                    LOGGER.debug("Assigned histHub ID %u to %s object %u",
                                 attr_value, bag.entity_name, bag.eav_id)
                    bag.set_attribute(self.HHBID_TRAIT_ATTR, attr_value)
                if attr_value:
                    change_batch.write_attribute(
                        main_eav_id,
                        anode.namid.id,
                        attr_value)

            # Write the relations
            for rnode in enode.rmap.values():
                for target_bag in bag.get_related(rnode.namid.name):
                    target_id = __recursive_data_insertion(
                        target_bag,
                        rnode.target,
                        level+1)
                    change_batch.write_relation(
                        main_eav_id,
                        rnode.namid.id,
                        target_id)

            return main_eav_id

        entity_span = self.main_entity_span(eav_bag.entity_name)
        return __recursive_data_insertion(eav_bag, entity_span.enode)

    def delete_histhub_object(self,
                              change_batch: ChangeBatch,
                              histhub_bag: EavBag) -> None:
        """Deletes an object in the main histHub area and all of its related
        sub-objects until the entity span "limits" are reached.

        If histhub_bag represents an object, all of its attributes and
        outgoing relations will be deleted, but the anchor object itself will
        remain "untouched".
        """
        def __recursive_data_deletion(bag, enode, level=0):
            # Do not touch anchor nodes downstream the root.
            if bag.entity_name in self.anchor_entities and level > 0:
                return

            if bag.entity_name not in self.anchor_entities:
                change_batch.delete_object(bag.eav_id)

            # Delete all attributes
            change_batch.delete_all_attributes(bag.eav_id)

            # Traverse the relations to further delete objects.
            for rnode in enode.rmap.values():
                for target_bag in bag.get_related(rnode.namid.name):
                    change_batch.delete_relation_instance(
                        bag.eav_id, rnode.namid.id, target_bag.eav_id)
                    __recursive_data_deletion(
                        target_bag, rnode.target, level + 1)

        # When deleting internal nodes, (all) incoming relations are cut.
        if histhub_bag.entity_name not in self.anchor_entities:
            change_batch.delete_all_backwards_relations(histhub_bag.eav_id)

        entity_span = self.main_entity_span(histhub_bag.entity_name)
        __recursive_data_deletion(histhub_bag, entity_span.enode)

    def _log_merge_action(self,
                          linking_entry: "LinkingMap.Entry",
                          action_text: str,
                          provider_id: "Optional[int]",
                          histhub_id: "Optional[int]") -> None:
        LOGGER.debug(
            "WRITER ACTION %s (stag=%s, main=%s, histhub=%s): "
            "%s (provider=%s)",
            linking_entry.entity_name,
            linking_entry.stag_id,
            linking_entry.eav_id,
            histhub_id,
            action_text,
            provider_id)
