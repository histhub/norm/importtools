from typing import Set

from eavlib.core.eav_bag import EavBag
from importtools.common.linking_map import LinkingMap


def anchor_trace(eav_bag: EavBag, anchor_entities: Set[str]) -> str:
    """Computes a canonical string representation of a EavBag instance,
    using:

        - the entity name,
        - the node id (if the entity name is in the anchor set),
        - a list of pairs attr_name="value" for all attributes sorted by
          attribute name,
        - a list of pairs rel_name=[related entities representations] lexi-
          cographically sorted, sorted again by relation name

    Example:
        Recipe@18329(level="low",items=[Ingredient(name="flour",qty="500 g")])

    Additionaly the trace is set as the 'anchor_trace' attribute in each bag.

    The web of sub-objects reachable from the passed EavBag can have cycles.
    When that is the case, a node already visited in the current DFS path to
    the root gets a dummy representation (class name together with the id in
    the anchor cases).
    """
    SEP = ","

    def _set_anchor_trace(bag: EavBag, anchors: Set[str],
                          path: Set[EavBag]) -> str:
        trace_entity = bag.entity_name
        trace_id = "@%u" % (bag.eav_id) if bag.entity_name in anchors else ""

        if bag in path:
            return trace_entity + trace_id

        new_path = path.union((bag,))

        trace_attrs = SEP.join(
            '%s="%s"' % (aname, aval.replace("\\", "\\\\").replace('"', '\\"'))
            for aname, aval in
            ((a, bag.get_attribute(a)) for a in sorted(bag.attributes))
            if aval)

        trace_rels = []
        for rname in sorted(bag.relations):
            related_bags = bag.get_related(rname)
            if not related_bags:
                continue
            trace_rels.append("{}=[{}]".format(
                rname,
                SEP.join(sorted(map(
                    lambda b: _set_anchor_trace(b, anchors, new_path),
                    related_bags)))))

        trace = "{}{}({}{}{})".format(
            trace_entity,
            trace_id,
            trace_attrs,
            SEP if trace_attrs and trace_rels else "",
            SEP.join(trace_rels))
        bag.anchor_trace = trace
        return trace

    return _set_anchor_trace(eav_bag, anchor_entities, set())


# TODO: Do it blindly without the anchor entities?
def translate_ids(
        eav_bag: EavBag,
        anchor_entities: Set[str],
        linking_map: LinkingMap) -> None:
    """Using a linking map translates the EAV ids of the anchor entities
    present in an EavBag instance and all its related instances.

    Raises RuntimeError if no translation is known for a node in the linking
    map.
    """

    def _translate_ids(
            bag: EavBag,
            anchors: Set[str],
            linking_map: LinkingMap,
            visited: Set[EavBag],
            path: Set[EavBag]) -> None:
        if bag in visited:
            return
        visited.add(bag)

        if bag in path:
            return
        new_path = path | {bag}

        if bag.entity_name in anchors:
            bag.eav_id = linking_map.main_node_id(bag.eav_id)

        for rname in bag.relations.keys():
            for child in bag.get_related(rname):
                _translate_ids(child, anchors, linking_map, visited, new_path)

    return _translate_ids(eav_bag, anchor_entities, linking_map, set(), set())
