from enum import Enum
from itertools import repeat
from collections import namedtuple
from typing import (
    Dict, ItemsView, Iterable, Iterator, List, Optional, Set, TextIO,
    ValuesView)


class LinkingMap:
    """Map between staging area eav ids and linking map entries that show the
    linkage state of that staging objects to found histhub counterparts.
    A linking entry might look like:

    +---------------------------------------------------------------------+
    | stag_id      =  1234        # eav id of object in staging area      |
    | entity_name  =  Person      # entity class name                     |
    | object_type  =  INFO        # obj has information or is reference   |
    | state        =  LINKED      # linking state                         |
    | eav_id       =  8947383     # eav id in main (or staging) area      |
    +---------------------------------------------------------------------+

    The keys in the map (staging id) and the EAV and HH ids in the entry are
    integers. Entity names are free strings; state and object type are enume-
    rated types.
    """
    ObjType = Enum('Type', ('info', 'ref'))
    State = Enum('State', ('unlinked', 'linked', 'intra_link', 'created'))

    class Entry:
        """Values of the linking map, containing information about histHub
        instances linked to a staging area one.
        """
        __slots__ = ('stag_id', 'entity_name', 'object_type', 'state',
                     'eav_id')

        def __init__(self,
                     staging_eav_id: int,
                     entity_name: str,
                     obj_type: "LinkingMap.ObjType",
                     state: "LinkingMap.State",
                     eav_id: Optional[int]) -> None:
            self.stag_id = int(staging_eav_id)
            self.entity_name = entity_name
            self.object_type = obj_type
            self.state = state
            self.eav_id = eav_id

        def __repr__(self) -> str:
            return "Entry(stag={}, entity={}, type={}, state={}, main={})" \
                .format(
                    self.stag_id, self.entity_name, self.object_type.name,
                    self.state.name, self.eav_id)

        def is_linked(self) -> bool:
            return self.state == LinkingMap.State.linked

        def set_as_linked(self, main_eav_id: int) -> None:
            self.state = LinkingMap.State.linked
            self.eav_id = main_eav_id

        def is_unlinked(self) -> bool:
            return self.state == LinkingMap.State.unlinked

        def set_as_unlinked(self) -> None:
            self.state = LinkingMap.State.unlinked
            self.eav_id = None

        def is_intra_link(self) -> bool:
            return self.state == LinkingMap.State.intra_link

        def set_as_intra_link(self, link_to_staging_id: int) -> None:
            """Sets this entry as an intra-link, meaning that this object is a
            reference to an object just being defined inside the current sub-
            mission (thus not known to histHub), and that will share the same
            histHub id that the object with the passed staging id.
            """
            self.state = LinkingMap.State.intra_link
            self.eav_id = link_to_staging_id

        def is_reference_object(self) -> bool:
            return self.object_type is LinkingMap.ObjType.ref

        def set_as_reference_object(self) -> None:
            self.object_type = LinkingMap.ObjType.ref

        def is_information_object(self) -> bool:
            return self.object_type is LinkingMap.ObjType.info

    __slots__ = ('_map')

    def __init__(self) -> None:
        self._map: Dict[int, "LinkingMap.Entry"] = dict()

    def __contains__(self, staging_id: int) -> bool:
        """Reports whether this linking map has a linking entry for the given
        staging id.
        """
        return staging_id in self._map

    def __getitem__(self, staging_id: int) -> "LinkingMap.Entry":
        return self._map[staging_id]

    def __str__(self) -> str:
        return '\n'.join(
            f'{stig}  -> {entry}' for stig, entry in self._map.items())

    def all_entries(self) -> ValuesView["LinkingMap.Entry"]:
        return self._map.values()

    def all_entity_names(self) -> Set[str]:
        return set(e.entity_name for e in self._map.values())

    def add(self,
            stag_id: int,
            entity_name: str,
            object_type: "LinkingMap.ObjType" = ObjType.info,
            state: "LinkingMap.State" = State.unlinked,
            eav_id: Optional[int] = None) -> None:
        """Adds an entry to the linking map, with object class and type
        information, but without any linking state for the moment.

        The default or unknown state is "unlinked".
        """
        if stag_id in self._map:
            raise RuntimeError(
                "linking map already contains entry for id=%d" % stag_id)

        self._map[stag_id] = LinkingMap.Entry(
            stag_id, entity_name, object_type, state, eav_id)

    def entries_for(self, **kwargs) -> Iterator["LinkingMap.Entry"]:
        """Query method for a linking map where three anded criteria can
        be specified: entity name, object type or state. Each one can
        be empty, have single element or a list.  For the ObjType and
        State enums, the string values are expected.

        Examples:

            .entries_for(state='intra_link')
            .entries_for(state='unlinked', otype='info')
            .entries_for(state='unlinked', otype='info', ename='Thesaurus')
            .entries_for(ename=['Thesaurus', 'ThesaurusConcept'])

        If wrong values are passed, you get silently an empty result.
        """
        KeyRecord = namedtuple('KeyRecord', 'everything extractor')
        valid_keys = {
            'ename': KeyRecord(self.all_entity_names(),
                               lambda e: e.entity_name),
            'otype': KeyRecord(set(LinkingMap.ObjType.__members__),
                               lambda e: e.object_type.name),
            'state': KeyRecord(set(LinkingMap.State.__members__),
                               lambda e: e.state.name),
            }

        wrong_keys = set(kwargs.keys()) - set(valid_keys)
        if wrong_keys:
            tmp = "unexpected keyword arguments: '{}'"
            raise RuntimeError(tmp.format("', '".join(wrong_keys)))

        # Make every keyword argument a list
        for key in valid_keys:
            value = kwargs.get(key, None)
            if value is None:
                kwargs[key] = list()
            elif type(value) is not list:
                kwargs[key] = list((value,))

        # Turn lists into sets.
        # When nothing was given, every possible value is used.
        for key in valid_keys:
            if kwargs[key]:
                kwargs[key] = set(kwargs[key])
            else:
                kwargs[key] = valid_keys[key].everything

        # Traverse to implement the given filter
        for entry in self._map.values():
            blist = []
            for key in valid_keys:
                blist.append(valid_keys[key].extractor(entry) in kwargs[key])

            if all(blist):
                yield entry

    def entries(self, entity_names: Optional[Iterable[str]] = None) \
            -> List["LinkingMap.Entry"]:
        """Deliver all entries in this linking map

                entity_names    Only entries whose entity name is in
                                this set are returned. If the set is
                                None, all map entries are returned.
        """
        if entity_names is None:
            return list(self._map.values())
        else:
            return list(filter(
                lambda v: v.entity_name in entity_names, self._map.values()))

    def entries_by_state(self, filter_state: "LinkingMap.State",
                         entity_names: Optional[Iterable[str]] = None) \
            -> List["LinkingMap.Entry"]:
        """Deliver the entries that are in the given state.

                filter_state    The state of the entries to return.

                entity_names    Only entries whose entity names are in
                                this set of entity names are returned.
                                If the set is None, all found entries
                                (in unlinked state) are returned.
        """
        if entity_names is None:
            def condition(v):
                return v.state == filter_state
        else:
            names_type = type(entity_names)
            if names_type is str:
                inclusion_set = set([entity_names])
            elif names_type in (set, list):
                inclusion_set = entity_names
            else:
                raise RuntimeError(
                    f"unexpected type '{names_type}' for entity_names")

            def condition(v):
                return (v.state == filter_state
                        and v.entity_name in inclusion_set)
        return list(filter(condition, self._map.values()))

    def linked_entries(self, entity_names: Optional[Iterable[str]] = None) \
            -> List["LinkingMap.Entry"]:
        return self.entries_by_state(LinkingMap.State.linked, entity_names)

    def unlinked_entries(self, entity_names: Optional[Iterable[str]] = None) \
            -> List["LinkingMap.Entry"]:
        return self.entries_by_state(LinkingMap.State.unlinked, entity_names)

    def created_entries(self, entity_names: Optional[Iterable[str]] = None) \
            -> List["LinkingMap.Entry"]:
        return self.entries_by_state(LinkingMap.State.created, entity_names)

    def unlinked_ids(self, entity_names: Optional[Iterable[str]] = None) \
            -> List[int]:
        """Deliver the keys (that is, staging eav ids) that are in unlinked
        state.

                entity_names    Only ids for entries whose entity names are
                                in this set of entity names are returned.
                                If the set is None, all found entries (in
                                unlinked state) are returned.
        """
        return [e.stag_id for e in self.unlinked_entries(entity_names)]

    def items(self) -> ItemsView[int, "LinkingMap.Entry"]:
        return self._map.items()

    def main_node_id(self, stag_id: int) -> Optional[int]:
        """Returns the eav id in the main repository that corresponds to the
        given staging eav id.
        """
        if stag_id in self._map:
            return self[stag_id].eav_id
        else:
            raise RuntimeError(f"unknown staging id '{stag_id}'")

    def dump(self, output_file: TextIO) -> None:
        """Dump this linking map content into a 6 columns TSV file like:

        stag_id  entity_name  object_type  state       eav_id
        100015   Person       ref          linked      800345
        100027   Person       info         unlinked
        100043   Person       ref          intra-link  100027
        100077   Place        ref          linked      801074
        100091   Place        info         linked      802033
        """
        template = '\t'.join(repeat('{}', len(LinkingMap.Entry.__slots__)))
        print(template.format(*LinkingMap.Entry.__slots__), file=output_file)
        for link_entry in self._map.values():
            print(template.format(
                link_entry.stag_id,
                link_entry.entity_name,
                link_entry.object_type.name,
                link_entry.state.name,
                link_entry.eav_id if link_entry.eav_id else ''
            ), file=output_file)

    @classmethod
    def read(cls, in_file: TextIO, skip_header: bool = True) -> "LinkingMap":
        """Reads a TSV file with the same structure produced by the dump method
        and returns a linking_map object.
        """
        skipped = skip_header
        self = cls()

        for line in in_file:
            if skipped:
                skipped = False
                continue

            data = line[:-1].split('\t')
            if data[3] in self.State.__members__:
                self.add(
                    int(data[0]),                       # staging id
                    data[1],                            # entity name
                    self.ObjType.__members__[data[2]],  # object_type
                    self.State.__members__[data[3]],    # entry state
                    int(data[4]) if data[4] else None)  # main eav id

        return self
