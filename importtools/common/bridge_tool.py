from itertools import chain

from eavlib.core.data_manager import DataManager
from eavlib.core.id_manager import SequenceIDManager


class BridgeTool:
    """Superclass for tools that access both the histhub main repository
    and the staging area for comparing or transfering information.
    """
    HHBID_TRAIT = "Identifiable"
    HHBID_TRAIT_ATTR = 'hhb_id'

    OWNED_TRAIT = "Owned"
    OWNED_TRAIT_REL = 'owners'

    # Entities that act as anchoring points in the graph of data for the
    # merging algorithm.
    # This variable should not be used directly, use BridgeTool#anchor_entities
    TOP_ANCHOR_ENTITIES = (
        'Identifiable',
        'DataSource',
        'Institution',
        'License',
        'Location',
        'Reference',
        )

    def __init__(self, histhub_graph, staging_graph):
        self._histhub_dm = DataManager(histhub_graph, SequenceIDManager(
            histhub_graph.connection, "object_id_seq"))
        self._histhub_dm.name = 'main'
        self._staging_dm = DataManager(staging_graph)
        self._staging_dm.name = 'staging'

        # Check that both data managers do work with the very same model
        self._stag_data_model = self._staging_dm.eav_model()
        self._main_data_model = self._histhub_dm.eav_model()
        if self._main_data_model != self._stag_data_model:
            raise RuntimeError('histHub and staging EAV models differ')

        self.anchor_entities = {e.name for e in chain.from_iterable(map(
            lambda n: self._main_data_model.entity(n).all_children,
            self.TOP_ANCHOR_ENTITIES))}

        self.identifiable_entities = {
            e.name for e in self._main_data_model.entity(
                self.HHBID_TRAIT).all_children}
