import datetime
import functools


def timed(logger):
    def decorator(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            start = datetime.datetime.now()
            res = func(*args, **kwargs)
            delta = (datetime.datetime.now() - start).total_seconds()

            logger.debug("TIME = %2dh %02d' %06.3f\"" % (
                delta // 3600, delta // 60 % 60, delta % 60))

            return res
        return wrapped
    return decorator
